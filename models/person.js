const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const personSchema = new Schema({

	name : {type: String, uppercase: true},
	address : {type: String},
	age : {type: Number},
	gender : {type: Boolean},
	created_at : {type: Date, select: false, default : Date.now},
	updated_at : {type: Date, select: false, default : Date.now}

});

personSchema.set('toObject', { getters: true });
personSchema.set('toJSON', { getters: true });


personSchema.pre('save', function(next) {
	var currentDate = new Date();
	this.updated_at = currentDate;
	next();
});


const Person = mongoose.model('Person', personSchema);

module.exports = Person;