const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const Person = require('./models/person');

mongoose.connect('mongodb://test:tester1@ds048878.mlab.com:48878/MongoLab-l');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());


app.get('/', async (req, res) => {
	
	try{
		let persons = await Person.find();
		res.status(200).json({persons})
	}catch(e){
		res.status(500).json(e);
	}

});

app.get('/:id', async (req, res) => {
	try{
		let id = req.params.id;
		let person = await person.findById(id);
		res.status(200).json({person})
	}catch(e){
		res.status(500).json(e);
	}
	
});


app.post('/', async (req, res) => {
	try{
		let person = new Person(req.body);
		await person.save();
		res.status(200).json({person})
	}catch(e){
		res.status(500).json(e);
	}
});

app.listen(3003, () => {
	console.log('Server listening on 3003 port');
});

